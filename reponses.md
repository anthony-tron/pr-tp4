### Que fait ce serveur ? Pourriez vous déduire le protocole de communication sous-jacent ?
Protocole : UDP.
Ce serveur envoie le fichier demandé par un client qui se connecte. 

### Pourquoi utilise-t-il un OutputStream et non pas un BufferedWriter ? 
La lecture de fichier se fait avec un objet de la classe `FileInputStream` et sa méthode `read(buffer: byte[])`. Il est donc naturel d'utiliser la classe `OutputStream` et sa méthode `write(buffer: byte[])` car les types sont directement compatibles (`byte[]`).

### Est-ce qu'on peut remplacer son BufferedReader par un InputStream ?
### Est-ce que ce serveur peut traiter plusieurs clients en même temps ?
### Que se passe-t-il côté serveur si le client ne lui envoie rien au bout de 20 sec ?
### Implémentez le client correspond. Utilisez ce dernier pour télécharger certains fichiers sur le serveur de votre enseignant(e) (demandez lui les noms des fichiers et l'adresse IP du serveur ainsi que le port).
### Modifiez cette application pour que le serveur envoie en plus, juste après la connexion d'un client, la liste des fichiers disponibles au téléchargement dans son répertoire de travail. Vous pouvez utiliser la classe File pour afficher le contenu d'un dossier.