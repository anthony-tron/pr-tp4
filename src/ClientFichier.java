import java.io.*;
import java.net.Socket;
import java.nio.file.Path;
import java.nio.file.Paths;

public class ClientFichier {

    final private int port;
    final private String host;

    public ClientFichier(int port, String host) {
        this.port = port;
        this.host = host;
    }

    public void getFile(Path path) {
        try {
            Socket socket = new Socket(host, port);
            BufferedWriter writer = new BufferedWriter(new OutputStreamWriter(socket.getOutputStream()));
            writer.write(path.toString());
            writer.newLine();
            writer.flush();

            InputStream inputStream = socket.getInputStream();
            byte[] buffer = new byte[512];
            int bytesCount;

            FileOutputStream out = new FileOutputStream("test");

            while ((bytesCount = inputStream.read(buffer)) != -1)
            {
                out.write(buffer, 0, bytesCount);
            }

        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public static void main(String[] args) {
        if (args.length != 1) {
            System.err.println("Usage: command PATH/TO/FILE");
            return;
        }

        Path path = Paths.get(args[0]);
        ClientFichier client = new ClientFichier(2000, "10.203.28.173");
        // adresse IP : 10.203.28.173 port :  2000 fichier : image.jpg

        System.out.println("Le client demande le fichier : " + path);
        client.getFile(path);
    }
}
